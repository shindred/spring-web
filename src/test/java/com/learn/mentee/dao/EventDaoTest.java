package com.learn.mentee.dao;

import com.learn.mentee.entity.Event;
import com.learn.mentee.storage.EventStorage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventDaoTest {

    @InjectMocks
    private EventDao eventDao; 

    @Mock
    private EventStorage eventStorageMock;

    @Spy
    private HashMap<Long, Event> eventMapSpy;

    @Mock
    private Event eventMock;

    @Mock
    private Date dateMock;

    @Before
    public void setUp() {
        when(eventMock.getTitle()).thenReturn("");
        when(eventMock.getId()).thenReturn(1L);
        when(eventMock.getDate()).thenReturn(dateMock);
        when(eventStorageMock.getEventMap()).thenReturn(eventMapSpy);
        eventMapSpy.put(1L, eventMock);
    }

    @Test
    public void getEventByIdTest() {
       assertEquals(Optional.of(eventMock), eventDao.getEventById(1L));
    }

    @Test
    public void getEventByTitleTest() {
        assertTrue(eventDao.getEventByTitle("", 0, 1).contains(eventMock));
    }

    @Test
    public void getEventsForDayTitle() {
        assertTrue(eventDao.getEventsForDay(dateMock, 0, 1).contains(eventMock));
    }

    @Test
    public void createEventTest() {
        Event event = new Event();
        assertEquals(event, eventDao.createEvent(event));
    }

    @Test
    public void updateEventTest() {
        assertEquals(eventMock, eventDao.updateEvent(eventMock));
    }

    @Test
    public void deleteEventTest() {
        assertTrue(eventDao.deleteEvent(1L));
    }
}
