package com.learn.mentee.dao;

import com.learn.mentee.entity.Event;
import com.learn.mentee.entity.Ticket;
import com.learn.mentee.entity.User;
import com.learn.mentee.entity.enumaration.TicketCategory;
import com.learn.mentee.storage.TicketStorage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicketDaoTest {

    @InjectMocks
    private TicketDao ticketDao;

    @Mock
    private TicketStorage ticketStorageMock;

    @Spy
    private HashMap<Long, Ticket> ticketMapSpy;

    @Mock
    private Ticket ticketMock;

    @Mock
    private User userMock;

    @Mock
    private Event eventMock;

    @Before
    public void setUp() {
        when(ticketStorageMock.getTicketMap()).thenReturn(ticketMapSpy);
        ticketMapSpy.put(1L, ticketMock);
        when(ticketMock.getUserId()).thenReturn(1L);
        when(userMock.getId()).thenReturn(1L);
    }

    @Test
    public void bookTicketTest() {
        assertNotNull(ticketDao.bookTicket(1L, 1L, 1, TicketCategory.STANDARD));
    }

    @Test
    public void getBookedTicketsByUserTest() {
        assertEquals(Collections.singletonList(ticketMock), ticketDao.getBookedTicketsByUser(userMock, 0, 1));
    }

    @Test
    public void getBookedTicketsByEventTest() {
        assertEquals(Collections.singletonList(ticketMock), ticketDao.getBookedTicketsByEvent(eventMock, 0, 1));
    }

    @Test
    public void cancelTicketTest() {
        assertTrue(ticketDao.cancelTicket(1L));
    }

    @Test
    public void getAllTicketsTest() {
        assertEquals(Collections.singletonList(ticketMock), ticketDao.getAllTickets());
    }

    @Test
    public void bookTicketsTest() {
        List<Ticket> expectedList = Collections.singletonList(ticketMock);
       assertEquals(expectedList, ticketDao.bookTickets(Collections.singletonList(ticketMock)));
    }
}