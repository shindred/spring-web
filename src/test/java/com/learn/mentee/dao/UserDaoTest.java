package com.learn.mentee.dao;

import com.learn.mentee.entity.User;
import com.learn.mentee.storage.UserStorage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDaoTest {

    @InjectMocks
    private UserDao userDao;

    @Mock
    private UserStorage userStorageMock;

    @Spy
    private HashMap<Long, User> userMapSpy;

    @Mock
    private User userMock;

    @Before
    public void setUp() {
        when(userStorageMock.getUserMap()).thenReturn(userMapSpy);
        userMapSpy.put(1L, userMock);
        when(userMock.getId()).thenReturn(1L);
    }

    @Test
    public void getUserByIdTest() {
        assertEquals(Optional.of(userMock), userDao.getUserById(1L));
    }

    @Test
    public void getUserByEmailTest() {
        when(userMock.getEmail()).thenReturn("");
        assertEquals(Optional.of(userMock), userDao.getUserByEmail(""));
    }

    @Test
    public void getUsersByNameTest() {
        when(userMock.getName()).thenReturn("");
        assertEquals(Collections.singletonList(userMock), userDao.getUsersByName("", 0, 1));
    }

    @Test
    public void createUserTest() {
        User user = new User();
        assertEquals(user, userDao.createUser(user));
    }

    @Test
    public void updateUserTest() {
        assertEquals(userMock, userDao.updateUser(userMock));
    }

    @Test
    public void deleteUserTest() {
        assertTrue(userDao.deleteUser(1L));
    }

    @Test
    public void getAllUsersTest() {
        assertEquals(Collections.singletonList(userMock), userDao.getAllUsers());
    }
}