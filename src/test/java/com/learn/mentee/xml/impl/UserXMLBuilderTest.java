package com.learn.mentee.xml.impl;

import com.learn.mentee.entity.User;
import com.learn.mentee.util.converter.impl.UserToXMLConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import java.io.OutputStream;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserXMLBuilderTest {

    @InjectMocks
    private UserXMLBuilder userXMLBuilder;

    @Mock
    private Jaxb2Marshaller marshaller;

    @Mock
    private UserToXMLConverter userToXMLConverter;

    @Mock
    private User userMock;

    @Mock
    private OutputStream outputStreamMock;

    @Test
    public void buildXmlTest() {
        userXMLBuilder.buildXml(userMock, outputStreamMock);
        verify(userToXMLConverter, times(1)).convert(any());
        verify(marshaller, times(1)).marshal(any(), any());
    }
}