package com.learn.mentee.service.impl;

import com.learn.mentee.dao.EventDao;
import com.learn.mentee.entity.Event;
import com.learn.mentee.validator.PaginationParameterExceptionThrower;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EventServiceImplTest {

    @InjectMocks
    private EventServiceImpl eventService;

    @Mock
    private EventDao eventDaoMock;

    @Mock
    private PaginationParameterExceptionThrower throwerMock;

    @Mock
    private Event eventMock;

    @Test
    public void getEventByIdTest() {
        when(eventDaoMock.getEventById(anyLong())).thenReturn(java.util.Optional.of(eventMock));
        eventService.getEventById(1L);
        verify(eventDaoMock, times(1)).getEventById(anyLong());
    }

    @Test
    public void getEventByTitleTest() {
        when(eventDaoMock.getEventByTitle(anyString(), anyInt(), anyInt())).thenReturn(Collections.singletonList(eventMock));
        eventService.getEventByTitle("", 1, 1);
        verify(eventDaoMock, times(1)).getEventByTitle(anyString(), anyInt(), anyInt());
    }

    @Test
    public void getEventsForDayTest() {
        when(eventDaoMock.getEventsForDay(any(), anyInt(), anyInt())).thenReturn(Collections.singletonList(eventMock));
        eventService.getEventsForDay(new Date(), 1, 1);
        verify(eventDaoMock, times(1)).getEventsForDay(any(), anyInt(), anyInt());
    }

    @Test
    public void createEventTest() {
        eventService.createEvent(eventMock);
        verify(eventDaoMock, times(1)).createEvent(any());
    }

    @Test
    public void updateEventTest() {
        eventService.updateEvent(eventMock);
        verify(eventDaoMock, times(1)).updateEvent(any());
    }

    @Test
    public void deleteEventTest() {
        eventService.deleteEvent(1L);
        verify(eventDaoMock, times(1)).deleteEvent(anyLong());
    }
}