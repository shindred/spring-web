package com.learn.mentee.service.impl;

import com.learn.mentee.dao.UserDao;
import com.learn.mentee.entity.User;
import com.learn.mentee.validator.PaginationParameterExceptionThrower;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserDao userDaoMock;

    @Mock
    private User userMock;

    @Mock
    private PaginationParameterExceptionThrower throwerMock;

    @Test
    public void getUserById() {
        when(userDaoMock.getUserById(anyLong())).thenReturn(Optional.of(userMock));
        userService.getUserById(1L);
        verify(userDaoMock, times(1)).getUserById(anyLong());
    }

    @Test
    public void getUserByExistentEmailTest() {
        when(userDaoMock.getUserByEmail(anyString())).thenReturn(Optional.of(userMock));
        userService.getUserByEmail("e");
        verify(userDaoMock, times(1)).getUserByEmail(anyString());
    }

    @Test
    public void getUsersByNameTest() {
        when(userDaoMock.getUsersByName(anyString(), anyInt(), anyInt())).thenReturn(Collections.singletonList(userMock));
        userService.getUsersByName("n", 1, 1);
        verify(userDaoMock, times(1)).getUsersByName(anyString(), anyInt(), anyInt());
    }

    @Test
    public void createUserTest() {
        userService.createUser(userMock);
        verify(userDaoMock, times(1)).createUser(any());
    }

    @Test
    public void updateUserTest() {
        userService.updateUser(userMock);
        verify(userDaoMock, times(1)).updateUser(any());
    }

    @Test
    public void deleteUserTest() {
        userService.deleteUser(1L);
        verify(userDaoMock, times(1)).deleteUser(anyLong());
    }

    @Test
    public void getAllUsersTest() {
        userService.getAllUsers();
        verify(userDaoMock, times(1)).getAllUsers();
    }
}