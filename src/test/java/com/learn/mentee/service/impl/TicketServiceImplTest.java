package com.learn.mentee.service.impl;

import com.learn.mentee.dao.TicketDao;
import com.learn.mentee.entity.Event;
import com.learn.mentee.entity.Ticket;
import com.learn.mentee.entity.User;
import com.learn.mentee.entity.enumaration.TicketCategory;
import com.learn.mentee.validator.PaginationParameterExceptionThrower;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceImplTest {

    @InjectMocks
    private TicketServiceImpl ticketService;

    @Mock
    private TicketDao ticketDaoMock;

    @Mock
    private Ticket ticketMock;

    @Mock
    private User userMock;

    @Mock
    private Event eventMock;

    @Mock
    private PaginationParameterExceptionThrower thrower;

    private final long id = 1L;
    private final TicketCategory ticketCategory = TicketCategory.STANDARD;

    @Test
    public void bookTicketTest() {
        ticketService.bookTicket(id, id, 1, ticketCategory);
        verify(ticketDaoMock, times(1)).bookTicket(anyLong(), anyLong(), anyInt(), any());
    }

    @Test
    public void getBookedTicketsByUserTest() {
        when(ticketDaoMock.getBookedTicketsByUser(any(), anyInt(), anyInt())).thenReturn(Collections.singletonList(ticketMock));
        ticketService.getBookedTicketsByUser(userMock, 1, 1);
        verify(ticketDaoMock, times(1)).getBookedTicketsByUser(any(), anyInt(), anyInt());
    }

    @Test
    public void getBookedTicketsByEventWithValidInputTest() {
        when(ticketDaoMock.getBookedTicketsByEvent(any(), anyInt(), anyInt())).thenReturn(Collections.singletonList(ticketMock));
        ticketService.getBookedTicketsByEvent(eventMock, 1, 1);
        verify(ticketDaoMock, times(1)).getBookedTicketsByEvent(any(), anyInt(), anyInt());
    }

    @Test
    public void cancelTicketTest() {
        ticketService.cancelTicket(id);
        verify(ticketDaoMock, times(1)).cancelTicket(anyLong());
    }

    @Test
    public void getAllTicketsTest() {
        ticketService.getAllTickets();
        verify(ticketDaoMock, times(1)).getAllTickets();
    }

    @Test
    public void bookTicketsTest() {
        ticketService.bookTickets(Collections.emptyList());
        verify(ticketDaoMock, times(1)).bookTickets(any());
    }
}