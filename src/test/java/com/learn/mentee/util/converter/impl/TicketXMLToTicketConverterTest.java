package com.learn.mentee.util.converter.impl;

import com.learn.mentee.entity.enumaration.TicketCategory;
import com.learn.mentee.entity.xml.TicketXML;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class TicketXMLToTicketConverterTest {

    @Test
    public void convertTest() {
        TicketXMLToTicketConverter converter = new TicketXMLToTicketConverter();
        TicketXML ticketXML = new TicketXML();
        ticketXML.setEventId(1L);
        ticketXML.setId(1L);
        ticketXML.setPlace(1);
        ticketXML.setEventId(1L);
        ticketXML.setTicketCategory(TicketCategory.STANDARD);
        assertNotNull(converter.convert(ticketXML));
    }
}