package com.learn.mentee.util.converter.impl;

import com.learn.mentee.entity.xml.UserXML;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserXMLToUserConverterTest {

    @Test
    public void convertTest() {
        UserXMLToUserConverter converter = new UserXMLToUserConverter();
        UserXML userXML = new UserXML();
        userXML.setId(1L);
        userXML.setName("");
        userXML.setEmail("");
        assertNotNull(converter.convert(userXML));
    }
}