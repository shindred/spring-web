package com.learn.mentee.util.converter.impl;

import com.learn.mentee.entity.User;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class UserToXMLConverterTest {

    @Test
    public void convertTest() {
        UserToXMLConverter converter = new UserToXMLConverter();
        User user = new User();
        user.setId(1L);
        user.setName("");
        user.setEmail("");
        assertNotNull(converter.convert(user));
    }
}