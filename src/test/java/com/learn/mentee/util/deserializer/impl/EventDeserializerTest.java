package com.learn.mentee.util.deserializer.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventDeserializerTest {

    @Spy
    private EventDeserializer eventDeserializer;

    @Test
    public void deserializeTest() {
        when(eventDeserializer.getPath()).thenReturn("");
        assertNotNull(eventDeserializer.deserialize());
    }
}