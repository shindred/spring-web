package com.learn.mentee.util.dataprovider.impl;

import com.learn.mentee.util.converter.impl.EventListToMapConverter;
import com.learn.mentee.util.deserializer.impl.EventDeserializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EventDataProviderTest {

    @InjectMocks
    private EventDataProvider eventDataProvider;

    @Mock
    private EventDeserializer eventDeserializerMock;

    @Mock
    private EventListToMapConverter eventListToMapConverterMock;

    @Test
    public void provideTest() {
        assertNotNull(eventDataProvider.provide());
        verify(eventDeserializerMock, times(1)).deserialize();
        verify(eventListToMapConverterMock, times(1)).convert(any());
    }
}