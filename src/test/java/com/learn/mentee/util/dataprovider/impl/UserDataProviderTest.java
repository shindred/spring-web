package com.learn.mentee.util.dataprovider.impl;

import com.learn.mentee.util.converter.impl.UserListToMapConverter;
import com.learn.mentee.util.deserializer.impl.UserDeserializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserDataProviderTest {

    @InjectMocks
    private UserDataProvider userDataProvider;

    @Mock
    private UserListToMapConverter converterMock;

    @Mock
    private UserDeserializer deserializerMock;

    @Test
    public void provideTest() {
        assertNotNull(userDataProvider.provide());
        verify(converterMock, times(1)).convert(any());
        verify(deserializerMock, times(1)).deserialize();
    }
}