package com.learn.mentee.util.dataprovider.impl;

import com.learn.mentee.util.converter.impl.TicketListToMapConverter;
import com.learn.mentee.util.deserializer.impl.TicketDeserializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TicketDataProviderTest {

    @InjectMocks
    private TicketDataProvider ticketDataProvider;

    @Mock
    private TicketListToMapConverter ticketListToMapConverterMock;

    @Mock
    private TicketDeserializer ticketDeserializerMock;

    @Test
    public void provideTest() {
        assertNotNull(ticketDataProvider.provide());
        verify(ticketListToMapConverterMock, times(1)).convert(any());
        verify(ticketDeserializerMock, times(1)).deserialize();
    }
}