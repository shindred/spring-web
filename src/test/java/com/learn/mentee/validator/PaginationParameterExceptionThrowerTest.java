package com.learn.mentee.validator;

import com.learn.mentee.exception.PaginationParameterException;
import org.junit.Test;

import static org.junit.Assert.*;

public class PaginationParameterExceptionThrowerTest {

    @Test(expected = PaginationParameterException.class)
    public void throwIfParametersAreLessThanOneTest() {
        PaginationParameterExceptionThrower thrower = new PaginationParameterExceptionThrower();
        thrower.throwIfParametersAreLessThanOne(-1, 1);
    }
}