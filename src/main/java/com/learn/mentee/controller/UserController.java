package com.learn.mentee.controller;

import com.learn.mentee.entity.User;
import com.learn.mentee.service.UserService;
import com.learn.mentee.storage.XmlUsersStorage;
import com.learn.mentee.util.exporter.impl.XMLToUserExporter;
import com.learn.mentee.util.exporter.impl.XMLToUsersExporter;
import com.learn.mentee.xml.impl.UserXMLBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Controller
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    private final UserXMLBuilder userXMLBuilder;

    private final XMLToUserExporter xmlToUserExporter;

    private final XMLToUsersExporter xmlToUsersExporter;

    @GetMapping("/{id}")

    public String getUserById(Model model, @PathVariable long id) {
        model.addAttribute("user", userService.getUserById(id));
        return "user";
    }

    @GetMapping("/xml/one")
    public void getOneUserXml(@RequestParam long id, HttpServletResponse response) throws IOException {
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=user" + id + ".xml";
        response.setHeader(headerKey, headerValue);
        userXMLBuilder.buildXml(userService.getUserById(id), response.getOutputStream());
    }

    @GetMapping("/preload")
    public String preloadTickets() {
        User user = xmlToUserExporter.exportUserFromXml();
        userService.createUser(user);
        return "redirect:/user/" + user.getId();
    }

    @GetMapping("/get/by-email")
    public String getUserByEmail(Model model, @RequestParam String email) {
        User foundUser = userService.getUserByEmail(email);
        model.addAttribute("user", foundUser);
        return "redirect:/user/" + foundUser.getId();
    }

    @GetMapping("/get/by-name")
    public String getUsersByNamePerPage(@RequestParam("name") String name,
                                        @RequestParam("pageNum") int pageNum,
                                        @RequestParam("pageSize") int pageSize,
                                        Model model) {
        model.addAttribute("users", userService.getUsersByName(name, pageSize, pageNum));
        return "users";
    }

    @PostMapping("/delete")
    public String deleteUserById(@RequestParam long id) {
        userService.deleteUser(id);
        return "redirect:/";
    }

    @PostMapping("/update")
    public String updateUserById(@RequestParam long id,
                                 @RequestParam String name,
                                 @RequestParam String email,
                                 Model model) {

        User user = new User(id, name, email);

        model.addAttribute("user", userService.updateUser(user));

        return "redirect:/user/" + id;
    }

    @PostMapping("/create")
    public String createUser(@RequestParam String name,
                             @RequestParam String email,
                             Model model) {

        User user = new User();
        user.setName(name);
        user.setEmail(email);

        User savedUser = userService.createUser(user);

        model.addAttribute("user", savedUser);

        return "redirect:/user/" + savedUser.getId();
    }

    @GetMapping("/xml/all")
    public void getAllUsersXml(HttpServletResponse response) throws IOException {
        List<User> users = userService.getAllUsers();

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users" + users.size() + ".xml";

        XmlUsersStorage xmlUsersStorage = new XmlUsersStorage(users);
        response.setHeader(headerKey, headerValue);
        xmlToUsersExporter.export(xmlUsersStorage, response.getOutputStream());
    }
}
