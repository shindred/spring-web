package com.learn.mentee.util.dataprovider.impl;

import com.learn.mentee.entity.Event;
import com.learn.mentee.util.converter.impl.EventListToMapConverter;
import com.learn.mentee.util.dataprovider.DataProvider;
import com.learn.mentee.util.deserializer.impl.EventDeserializer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class EventDataProvider implements DataProvider<Event> {

    private final EventDeserializer eventDeserializer;
    private final EventListToMapConverter eventListToMapConverter;

    @Override
    public Map<Long, Event> provide() {
        List<Event> eventList = eventDeserializer.deserialize();
        return eventListToMapConverter.convert(eventList);
    }
}
