package com.learn.mentee.util.dataprovider.impl;

import com.learn.mentee.entity.Ticket;
import com.learn.mentee.util.converter.impl.TicketListToMapConverter;
import com.learn.mentee.util.dataprovider.DataProvider;
import com.learn.mentee.util.deserializer.impl.TicketDeserializer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class TicketDataProvider implements DataProvider<Ticket> {

    private final TicketListToMapConverter ticketListToMapConverter;
    private final TicketDeserializer ticketDeserializer;

    @Override
    public Map<Long, Ticket> provide() {
        List<Ticket> ticketList = ticketDeserializer.deserialize();
        return ticketListToMapConverter.convert(ticketList);
    }
}
