package com.learn.mentee.util.dataprovider;

import java.util.Map;

public interface DataProvider<E> {
    /**
     * Provides data for the E entity storage
     *
     * @return Map of predefined data
     */
    Map<Long, E> provide();
}
