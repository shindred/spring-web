package com.learn.mentee.util.dataprovider.impl;

import com.learn.mentee.entity.User;
import com.learn.mentee.util.converter.impl.UserListToMapConverter;
import com.learn.mentee.util.dataprovider.DataProvider;
import com.learn.mentee.util.deserializer.impl.UserDeserializer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class UserDataProvider implements DataProvider<User> {

    private final UserListToMapConverter converter;

    private final UserDeserializer deserializer;

    @Override
    public Map<Long, User> provide() {
        List<User> userList = deserializer.deserialize();
        return converter.convert(userList);
    }
}
