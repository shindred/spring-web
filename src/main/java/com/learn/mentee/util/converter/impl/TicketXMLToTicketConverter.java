package com.learn.mentee.util.converter.impl;

import com.learn.mentee.entity.Ticket;
import com.learn.mentee.entity.xml.TicketXML;
import com.learn.mentee.util.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TicketXMLToTicketConverter implements Converter<TicketXML, Ticket> {

    @Override
    public Ticket convert(TicketXML ticketXML) {
        Ticket ticket = new Ticket();
        ticket.setId(ticketXML.getId());
        ticket.setEventId(ticketXML.getEventId());
        ticket.setUserId(ticketXML.getUserId());
        ticket.setTicketCategory(ticketXML.getTicketCategory());
        ticket.setPlace(ticketXML.getPlace());
        return ticket;
    }
}
