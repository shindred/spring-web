package com.learn.mentee.util.importer;

public interface Importer<O> {
    /**
     * Getting O object from predefined XML file
     *
     * @return O object
     */
    O doImport();
}