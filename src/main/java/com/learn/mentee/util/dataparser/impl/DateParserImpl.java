package com.learn.mentee.util.dataparser.impl;

import com.learn.mentee.util.dataparser.DateParser;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@Log4j2
public class DateParserImpl implements DateParser {

    @Override
    public Date parseDate(String dateToParse) {

        String pattern = "yyyy-MM-dd";
        SimpleDateFormat DateFor = new SimpleDateFormat(pattern);

        Date parsedDate;
        try {
            parsedDate = DateFor.parse(dateToParse);
        } catch (ParseException e) {
            parsedDate = new Date();
            log.warn("Can't parse date!");
        }

        return parsedDate;
    }
}
