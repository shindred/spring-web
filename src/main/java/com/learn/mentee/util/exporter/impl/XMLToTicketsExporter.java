package com.learn.mentee.util.exporter.impl;

import com.learn.mentee.entity.Ticket;
import com.learn.mentee.storage.XmlTicketsStorage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

@Component
public class XMLToTicketsExporter {

    @Value("${path.resources}${path.xml.tickets}")
    private String path;

    @XmlElements({
            @XmlElement(name = "Ticket", type = Ticket.class)
    })
    public void export(XmlTicketsStorage tickets) {
        JAXBContext jc = null;
        try {
            jc = JAXBContext.newInstance(XmlTicketsStorage.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(tickets,
                    new StreamResult(new FileOutputStream(path)));
        } catch (JAXBException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}