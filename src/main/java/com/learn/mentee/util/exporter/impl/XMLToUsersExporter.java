package com.learn.mentee.util.exporter.impl;

import com.learn.mentee.entity.User;
import com.learn.mentee.storage.XmlUsersStorage;
import com.learn.mentee.util.exporter.Exporter;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.transform.stream.StreamResult;
import java.io.OutputStream;

@Component
public class XMLToUsersExporter implements Exporter<XmlUsersStorage> {

    @XmlElements({
            @XmlElement(name = "User",
                    type = User.class)
    })
    @Override
    public void export(XmlUsersStorage users,
                       OutputStream outputStream) {
        JAXBContext jc;
        try {
            jc = JAXBContext.newInstance(XmlUsersStorage.class);
            Marshaller marshaller = jc.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(users, new StreamResult(outputStream));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}