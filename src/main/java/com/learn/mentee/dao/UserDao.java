package com.learn.mentee.dao;

import com.learn.mentee.entity.User;
import com.learn.mentee.exception.UserNotFoundException;
import com.learn.mentee.storage.UserStorage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class UserDao {

    private final UserStorage userStorage;

    /**
     * Gets user by its id.
     *
     * @return Optional of found user or
     * or empty Optional if user was not found
     */
    public Optional<User> getUserById(long userId) {
        return Optional.ofNullable(userStorage.getUserMap().get(userId));
    }

    /**
     * Gets user by it's email.
     *
     * @return Optional of found user
     * or empty Optional if user was not found
     */
    public Optional<User> getUserByEmail(String email) {
        return userStorage.getUserMap().values().stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst();
    }

    /**
     * Get list of users by matching name.
     *
     * @param name Users name.
     * @param from Start user to get from whole found list
     * @param to   End user to get from whole found list
     * @return List of users
     */
    public List<User> getUsersByName(String name, int from, int to) {
        List<User> usersByName = userStorage.getUserMap().values().stream()
                .filter(u -> u.getName().equals(name))
                .collect(Collectors.toList());

        return usersByName.subList(from, Math.min(to, usersByName.size()));
    }

    /**
     * Creates new user.
     *
     * @param user User data.
     * @return Created user
     * @throws RuntimeException If equivalent user already exists
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public User createUser(User user) {
        userStorage.getUserMap().keySet().stream()
                .max(Long::compareTo)
                .ifPresent(x -> user.setId(x + 1));

        User savedUser = userStorage.getUserMap().putIfAbsent(user.getId(), user);

        if (savedUser != null) {
            throw new RuntimeException("User with selected id already exists!");
        }

        return user;
    }

    /**
     * Updates user using given data.
     *
     * @param user User data for update. Should have id set.
     * @return Updated User object.
     * @throws UserNotFoundException If user with input id was not found
     */
    public User updateUser(User user) {
        return Optional
                .ofNullable(userStorage.getUserMap()
                        .computeIfPresent(user.getId(), (k, v) -> {
                            v.setName(user.getName());
                            v.setEmail(user.getEmail());
                            return v;
                        }))
                .orElseThrow(UserNotFoundException::new);
    }

    /**
     * Deletes user by its id.
     *
     * @param userId User id.
     * @return Flag that shows whether user has been deleted.
     */
    public boolean deleteUser(long userId) {
        return userStorage.getUserMap().remove(userId) != null;
    }

    /**
     * Get list of all existed users
     *
     * @return List of users or empty list if nothing was found.
     */
    public List<User> getAllUsers() {
        return new ArrayList<>(userStorage.getUserMap().values());
    }
}