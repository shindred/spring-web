package com.learn.mentee.dao;

import com.learn.mentee.entity.Event;
import com.learn.mentee.entity.Ticket;
import com.learn.mentee.entity.User;
import com.learn.mentee.entity.enumaration.TicketCategory;
import com.learn.mentee.storage.TicketStorage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class TicketDao {

    private final TicketStorage ticketStorage;

    /**
     * Book ticket for a specified event on behalf of specified user.
     *
     * @param userId         User id
     * @param eventId        Event id
     * @param place          Place number
     * @param ticketCategory Ticket category
     * @return Created ticket
     * @throws RuntimeException If equivalent ticket already exists.
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public Ticket bookTicket(long userId, long eventId, int place, TicketCategory ticketCategory) {
        Ticket ticket = new Ticket();
        ticketStorage.getTicketMap().keySet()
                .stream()
                .max(Long::compareTo)
                .ifPresent(x -> ticket.setId(x + 1));
        ticket.setUserId(userId);
        ticket.setEventId(eventId);
        ticket.setPlace(place);
        ticket.setTicketCategory(ticketCategory);

        Ticket savedTicket = ticketStorage.getTicketMap().putIfAbsent(ticket.getId(), ticket);

        if (savedTicket != null) {
            throw new RuntimeException("Ticket already exists!");
        }

        return ticket;
    }

    /**
     * Get all booked tickets for specified user
     *
     * @param user User
     * @param from Start ticket to get from whole found list
     * @param to   End ticket to get from whole found list
     * @return List of found tickets with amount that depends on 'from' and 'to' params
     */
    public List<Ticket> getBookedTicketsByUser(User user, int from, int to) {
        List<Ticket> ticketsByUser = ticketStorage.getTicketMap().values().stream()
                .filter(t -> t.getUserId() == user.getId())
                .collect(Collectors.toList());

        return ticketsByUser.subList(from, Math.min(to, ticketsByUser.size()));
    }

    /**
     * Get all booked tickets for specified event
     *
     * @param event Event
     * @param from  Start ticket to get from whole found list
     * @param to    End ticket to get from whole found list
     * @return List of found tickets with amount that depends on 'from' and 'to' params
     */
    public List<Ticket> getBookedTicketsByEvent(Event event, int from, int to) {
        List<Ticket> ticketsByEvent = ticketStorage.getTicketMap().values().stream()
                .filter(t -> t.getEventId() == event.getId())
                .collect(Collectors.toList());

        return ticketsByEvent.subList(from, Math.min(to, ticketsByEvent.size()));
    }

    /**
     * Cancel ticket by it's id
     *
     * @param ticketId Ticket id
     * @return Flag that shows whether ticket has been deleted.
     */
    public boolean cancelTicket(long ticketId) {
        return ticketStorage.getTicketMap().remove(ticketId) != null;
    }

    /**
     * Get list of all existed tickets
     *
     * @return List of tickets or empty list if nothing was found.
     */
    public List<Ticket> getAllTickets() {
        return new ArrayList<>(ticketStorage.getTicketMap().values());
    }

    /**
     * Book tickets for a specified events on behalf of specified users.
     *
     * @param tickets The list of tickets
     * @return List of created tickets.
     * @throws RuntimeException If at least one equivalent ticket from list  already exists.
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public List<Ticket> bookTickets(List<Ticket> tickets) {
        tickets.forEach(t -> {
            ticketStorage.getTicketMap().keySet().stream()
                    .max(Long::compareTo)
                    .ifPresent(l -> t.setId(l + 1));
            if (ticketStorage.getTicketMap().putIfAbsent(t.getId(), t) != null) {
                throw new RuntimeException("Ticket already exists");
            }
        });
        return tickets;
    }
}
