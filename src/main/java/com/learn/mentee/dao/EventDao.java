package com.learn.mentee.dao;

import com.learn.mentee.entity.Event;
import com.learn.mentee.exception.EventNotFoundException;
import com.learn.mentee.storage.EventStorage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class EventDao {

    private final EventStorage eventStorage;

    /**
     * Gets event by its id.
     *
     * @param eventId Event id
     * @return Optional of found event
     * or empty optional if event was not found
     */
    public Optional<Event> getEventById(long eventId) {
        return Optional.ofNullable(eventStorage.getEventMap().get(eventId));
    }

    /**
     * Get list of events by matching title.
     *
     * @param title Event title
     * @param from  Start event to get from whole found list
     * @param to    End event to get from whole found list
     * @return List of found events with amount that depends on 'from' and 'to' params
     */
    public List<Event> getEventByTitle(String title, int from, int to) {
        List<Event> events = eventStorage.getEventMap().values().stream()
                .filter(e -> e.getTitle().equals(title))
                .collect(Collectors.toList());

        return events.subList(from, Math.min(to, events.size()));
    }

    /**
     * Get list of events for specified day.
     *
     * @param day  Date object from which day information is extracted.
     * @param from Start event to get from whole found list
     * @param to   End event to get from whole found list
     * @return List of found events with amount that depends on 'from' and 'to' params
     */
    public List<Event> getEventsForDay(Date day, int from, int to) {
        List<Event> eventsForDay = eventStorage.getEventMap().values().stream()
                .filter(e -> e.getDate().equals(day))
                .collect(Collectors.toList());

        return eventsForDay.subList(from, Math.min(to, eventsForDay.size()));
    }

    /**
     * Creates new event
     *
     * @param event Event object
     * @return Created event
     * @throws RuntimeException if equivalent event already exists
     */
    @Transactional(rollbackFor = RuntimeException.class)
    public Event createEvent(Event event) {
        eventStorage.getEventMap().keySet().stream()
                .max(Long::compareTo)
                .ifPresent(x -> event.setId(x + 1));
        Event savedEvent = eventStorage.getEventMap().putIfAbsent(event.getId(), event);

        if (savedEvent != null) {
            throw new RuntimeException("Event with selected id already exists!");
        }

        return event;
    }

    /**
     * Updates event using given data.
     *
     * @param event Event data for update
     * @return Update event
     * @throws EventNotFoundException If event with input id was not found
     */
    public Event updateEvent(Event event) {
        return Optional.ofNullable(eventStorage.getEventMap()
                .computeIfPresent(event.getId(), (k, v) -> {
                    v.setDate(event.getDate());
                    v.setTitle(event.getTitle());
                    return v;
                }))
                .orElseThrow(EventNotFoundException::new);
    }

    /**
     * Deletes event by its id.
     *
     * @param eventId Event id.
     * @return Flag that shows whether event has been deleted.
     */
    public boolean deleteEvent(long eventId) {
        return eventStorage.getEventMap().remove(eventId) != null;
    }
}
