package com.learn.mentee.validator;

import com.learn.mentee.exception.PaginationParameterException;
import org.springframework.stereotype.Component;

@Component
public class PaginationParameterExceptionThrower {

    public void throwIfParametersAreLessThanOne(int firstParameter, int secondParameter) {
        if (firstParameter < 1 || secondParameter < 1) {
            throw new PaginationParameterException();
        }
    }
}
