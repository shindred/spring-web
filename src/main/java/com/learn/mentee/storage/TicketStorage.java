package com.learn.mentee.storage;

import com.learn.mentee.entity.Ticket;
import com.learn.mentee.util.dataprovider.impl.TicketDataProvider;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class TicketStorage {

    @Getter
    private Map<Long, Ticket> ticketMap;

    private final TicketDataProvider ticketDataProvider;

    @PostConstruct
    private void init() {
        ticketMap = ticketDataProvider.provide();
    }
}
