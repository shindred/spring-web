package com.learn.mentee.storage;

import com.learn.mentee.entity.User;
import com.learn.mentee.util.dataprovider.impl.UserDataProvider;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class UserStorage {
    @Getter
    private Map<Long, User> userMap;

    private final UserDataProvider userDataProvider;

    @PostConstruct
    public void init() {
        userMap =  userDataProvider.provide();
    }
}
