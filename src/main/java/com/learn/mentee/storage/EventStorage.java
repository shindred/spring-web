package com.learn.mentee.storage;

import com.learn.mentee.entity.Event;
import com.learn.mentee.util.dataprovider.impl.EventDataProvider;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class EventStorage {
    @Getter
    private Map<Long, Event> eventMap;

    private final EventDataProvider eventDataProvider;

    @PostConstruct
    private void init() {
        eventMap = eventDataProvider.provide();
    }
}
