package com.learn.mentee.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserXML {

    @XmlElement
    private long id;
    @XmlElement
    private String name;
    @XmlElement
    private String email;

    public UserXML() {
    }

    public UserXML(long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }
}
