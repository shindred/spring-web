package com.learn.mentee.entity;

import com.learn.mentee.entity.enumaration.TicketCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Ticket implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;
    private long eventId;
    private long userId;
    private TicketCategory ticketCategory;
    private int place;
}
