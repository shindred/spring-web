package com.learn.mentee.exception;

public class PaginationParameterException extends IllegalArgumentException {
    public PaginationParameterException() {
        super("Page size or number cannot be less than one!");
    }
}
