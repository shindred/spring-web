package com.learn.mentee.exception;

import java.util.NoSuchElementException;

public class TicketNotFoundException extends NoSuchElementException {
    public TicketNotFoundException() {
        super("Ticket not found");
    }
}
