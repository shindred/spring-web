package com.learn.mentee.exception;

import java.util.NoSuchElementException;

public class EventNotFoundException extends NoSuchElementException {
    public EventNotFoundException() {
        super("Event not found");
    }
}
