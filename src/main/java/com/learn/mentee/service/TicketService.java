package com.learn.mentee.service;

import com.learn.mentee.entity.Event;
import com.learn.mentee.entity.Ticket;
import com.learn.mentee.entity.User;
import com.learn.mentee.entity.enumaration.TicketCategory;
import com.learn.mentee.exception.PaginationParameterException;
import com.learn.mentee.exception.TicketNotFoundException;

import java.util.List;

public interface TicketService {

    /**
     * Book ticket for a specified event on behalf of specified user.
     *
     * @param userId         User Id.
     * @param eventId        Event Id.
     * @param place          Place number.
     * @param ticketCategory Ticket category.
     * @return Created ticket object.
     */
    Ticket bookTicket(long userId, long eventId, int place, TicketCategory ticketCategory);

    /**
     * Get all booked tickets for specified user.
     *
     * @param user     User
     * @param pageSize Number of tickets to return on a page.
     * @param pageNum  Number of the page to return.
     * @return List of Ticket objects
     * @throws TicketNotFoundException      if tickets were not found..
     * @throws PaginationParameterException if pageSize or pageNum is less than zero
     */
    List<Ticket> getBookedTicketsByUser(User user, int pageSize, int pageNum);

    /**
     * Get all booked tickets for specified event.
     *
     * @param event    Event
     * @param pageSize Number of tickets to return on a page.
     * @param pageNum  Number of the page to return.
     * @return List of Ticket objects.
     * @throws TicketNotFoundException      if tickets were not found..
     * @throws PaginationParameterException if pageSize or pageNum is less than zero
     */
    List<Ticket> getBookedTicketsByEvent(Event event, int pageSize, int pageNum);

    /**
     * Cancel ticket by it's id
     *
     * @param ticketId Ticket id
     * @return Flag that shows whether ticket has been deleted.
     */
    boolean cancelTicket(long ticketId);

    /**
     * Get list of all existed tickets
     *
     * @return List of tickets or empty list if nothing was found.
     */
    List<Ticket> getAllTickets();

    /**
     * Book tickets for a specified events on behalf of specified users.
     *
     * @param tickets The List of tickets
     * @return List of created tickets.
     */
    List<Ticket> bookTickets(List<Ticket> tickets);
}
