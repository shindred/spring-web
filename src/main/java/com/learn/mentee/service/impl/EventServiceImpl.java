package com.learn.mentee.service.impl;

import com.learn.mentee.dao.EventDao;
import com.learn.mentee.entity.Event;
import com.learn.mentee.exception.EventNotFoundException;
import com.learn.mentee.service.EventService;
import com.learn.mentee.validator.PaginationParameterExceptionThrower;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

    private final EventDao eventDao;

    private final PaginationParameterExceptionThrower thrower;

    @Override
    public Event getEventById(long eventId) {
        return eventDao.getEventById(eventId)
                .orElseThrow(EventNotFoundException::new);
    }

    @Override
    public List<Event> getEventByTitle(String title, int pageSize, int pageNum) {

        thrower.throwIfParametersAreLessThanOne(pageSize, pageNum);

        int from = pageSize * (pageNum - 1);
        int to = pageNum * pageSize;

        return Stream.of(eventDao.getEventByTitle(title, from, to))
                .filter(l -> from < l.size())
                .findAny()
                .orElseThrow(EventNotFoundException::new);
    }

    @Override
    public List<Event> getEventsForDay(Date day, int pageSize, int pageNum) {

        thrower.throwIfParametersAreLessThanOne(pageSize, pageNum);

        int from = pageSize * (pageNum - 1);
        int to = pageNum * pageSize;

        return Stream.of(eventDao.getEventsForDay(day, from, to))
                .filter(l -> from < l.size())
                .findAny()
                .orElseThrow(EventNotFoundException::new);
    }

    @Override
    public Event createEvent(Event event) {
        return eventDao.createEvent(event);
    }

    @Override
    public Event updateEvent(Event event) {
        return eventDao.updateEvent(event);
    }

    @Override
    public boolean deleteEvent(long eventId) {
        return eventDao.deleteEvent(eventId);
    }
}