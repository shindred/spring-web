package com.learn.mentee.service.impl;

import com.learn.mentee.dao.TicketDao;
import com.learn.mentee.entity.Event;
import com.learn.mentee.entity.Ticket;
import com.learn.mentee.entity.User;
import com.learn.mentee.entity.enumaration.TicketCategory;
import com.learn.mentee.exception.TicketNotFoundException;
import com.learn.mentee.service.TicketService;
import com.learn.mentee.validator.PaginationParameterExceptionThrower;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketDao ticketDao;

    private final PaginationParameterExceptionThrower paginationParameterExceptionThrower;

    @Override
    public Ticket bookTicket(long userId, long eventId, int place, TicketCategory ticketCategory) {
        return ticketDao.bookTicket(userId, eventId, place, ticketCategory);
    }

    @Override
    public List<Ticket> getBookedTicketsByUser(User user, int pageSize, int pageNum) {

        paginationParameterExceptionThrower.throwIfParametersAreLessThanOne(pageSize, pageNum);

        int from = pageSize * (pageNum - 1);
        int to = pageNum * pageSize;

        return Stream.of(ticketDao.getBookedTicketsByUser(user, from, to))
                .filter(l -> from < l.size())
                .findAny()
                .orElseThrow(TicketNotFoundException::new);
    }

    @Override
    public List<Ticket> getBookedTicketsByEvent(Event event, int pageSize, int pageNum) {

        paginationParameterExceptionThrower.throwIfParametersAreLessThanOne(pageSize, pageNum);

        int from = pageSize * (pageNum - 1);
        int to = pageNum * pageSize;

        return Stream.of(ticketDao.getBookedTicketsByEvent(event, from, to))
                .filter(l -> from < l.size())
                .findAny()
                .orElseThrow(TicketNotFoundException::new);
    }

    @Override
    public boolean cancelTicket(long ticketId) {
        return ticketDao.cancelTicket(ticketId);
    }

    @Override
    public List<Ticket> getAllTickets() {
        return ticketDao.getAllTickets();

    }

    @Override
    public List<Ticket> bookTickets(List<Ticket> tickets) {
        return ticketDao.bookTickets(tickets);
    }
}
