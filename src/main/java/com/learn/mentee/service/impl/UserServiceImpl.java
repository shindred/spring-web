package com.learn.mentee.service.impl;

import com.learn.mentee.dao.UserDao;
import com.learn.mentee.entity.User;
import com.learn.mentee.exception.UserNotFoundException;
import com.learn.mentee.service.UserService;
import com.learn.mentee.validator.PaginationParameterExceptionThrower;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    private final PaginationParameterExceptionThrower paginationParameterExceptionThrower;

    @Override
    public User getUserById(long userId) {
        return userDao.getUserById(userId)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User getUserByEmail(String email) {
        return userDao.getUserByEmail(email)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<User> getUsersByName(String name, int pageSize, int pageNum) {

        paginationParameterExceptionThrower.throwIfParametersAreLessThanOne(pageSize, pageNum);

        int from = pageSize * (pageNum - 1);
        int to = pageSize * pageNum;

        return Stream.of(userDao.getUsersByName(name, from, to))
                .filter(l -> from < l.size())
                .findAny()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User createUser(User user) {
        return userDao.createUser(user);
    }

    @Override
    public User updateUser(User user) {
        return userDao.updateUser(user);
    }

    @Override
    public boolean deleteUser(long userId) {
        return userDao.deleteUser(userId);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }
}